package yanhk;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 */
@Controller
@RequestMapping("/demo")
public class DemoController {

    /**
     * url: http://localhost:8080/demo/handle01
     */
//    @RequestMapping("/handle01")
    @GetMapping("/handle01")
    public ModelAndView handle01(@RequestParam("id") Integer id) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("id",id);
        modelAndView.setViewName("success");
        return modelAndView;
    }


}
